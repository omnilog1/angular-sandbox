import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AppRoutingModule } from './app-routing.module';
import { HeaderModule } from './app-header/app-header.module';
import { AppContainerModule } from './app-container/app-container.module';
import { FooterModule } from './app-footer/app-footer.module';
import { HomeModule } from './app-container/home/home.module';
import { NotesModule } from './app-container/notes/notes/notes.module';
import { NoteListModule } from './app-container/notes/note-list/note-list.module';
import { NoteInputModule } from './app-container/notes/note-input/note-input.module';
import { PageNotFoundModule } from './app-container/errors/page-not-found/page-not-found.module';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppRoutingModule,
        AppContainerModule,
        FooterModule,
        HeaderModule,
        HomeModule,
        NoteInputModule,
        NoteListModule,
        NotesModule,
        PageNotFoundModule
      ],
      declarations: [AppComponent]
    }).compileComponents();
  }));

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angular-sandbox'`, () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angular-sandbox');
  });

  it('should render the component', () => {
    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.app').innerHTML).toContain('<app-header ');
    expect(compiled.querySelector('.app').innerHTML).toContain('<app-footer ');
    expect(compiled.querySelector('.app').innerHTML).toContain(
      '<app-container '
    );
  });
});
