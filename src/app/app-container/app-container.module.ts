import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppContainerComponent } from './app-container.component';

@NgModule({
  imports: [RouterModule, CommonModule],
  declarations: [AppContainerComponent],
  exports: [AppContainerComponent]
})
export class AppContainerModule {}
