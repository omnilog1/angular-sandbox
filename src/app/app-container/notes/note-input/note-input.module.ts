import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NotesService } from '../../../../services/notes.service';

import { NoteInputComponent } from './note-input.component';

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [NoteInputComponent],
  exports: [NoteInputComponent],
  providers: [NotesService]
})
export class NoteInputModule {}
