import { Component, OnInit } from '@angular/core';
import { NotesService } from '../../../../services/notes.service';

@Component({
  selector: 'app-note-input',
  templateUrl: './note-input.component.html',
  styleUrls: ['./note-input.component.scss'],
})
export class NoteInputComponent implements OnInit {
  private inputNote: string;

  constructor(private data: NotesService) {}

  addNote = () => {
    if (this.inputNote.trim().length > 0) {
      this.data.addNote(this.inputNote.trim());
    }
    this.inputNote = '';
  };

  ngOnInit() {
    this.data.currentNotes.subscribe();
  }
}
