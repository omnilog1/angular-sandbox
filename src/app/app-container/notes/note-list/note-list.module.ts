import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotesService } from '../../../../services/notes.service';

import { NoteListComponent } from './note-list.component';

@NgModule({
  imports: [CommonModule],
  declarations: [NoteListComponent],
  exports: [NoteListComponent],
  providers: [NotesService]
})
export class NoteListModule {}
