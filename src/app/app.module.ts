import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NotesService } from '../services/notes.service';

import { AppRoutingModule } from './app-routing.module';
import { HeaderModule } from './app-header/app-header.module';
import { FooterModule } from './app-footer/app-footer.module';
import { AppContainerModule } from './app-container/app-container.module';
import { NotesModule } from './app-container/notes/notes/notes.module';
import { NoteListModule } from './app-container/notes/note-list/note-list.module';
import { NoteInputModule } from './app-container/notes/note-input/note-input.module';
import { HomeModule } from './app-container/home/home.module';
import { PageNotFoundModule } from './app-container/errors/page-not-found/page-not-found.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    AppContainerModule,
    FooterModule,
    FormsModule,
    HeaderModule,
    HomeModule,
    NoteInputModule,
    NoteListModule,
    NotesModule,
    PageNotFoundModule
  ],
  providers: [NotesService],
  bootstrap: [AppComponent]
})
export class AppModule {}
