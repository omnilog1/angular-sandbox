import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Note } from '../models/notes.interface';
import uuid from 'uuid';

@Injectable()
export class NotesService {
  private notes: Note[];
  private observableNotes: BehaviorSubject<Note[]>;

  constructor() {
    this.notes = new Array();
    this.observableNotes = new BehaviorSubject(this.notes);
  }

  get currentNotes() {
    return this.observableNotes.asObservable();
  }

  addNote = (title: string) => {
    if (title.trim().length > 0) {
      const note = {
        id: uuid(),
        title,
        isComplete: false,
      };
      this.notes.push(note);
    }
    this.observableNotes.next([...this.notes]);
  };

  deleteNote = (id: number) => {
    this.notes = this.notes.filter((note) => note.id !== id);
    this.observableNotes.next([...this.notes]);
  };

  toggleCompleteNoteState = (id: number) => {
    this.notes = this.notes.map((note) => {
      if (note.id === id) {
        note.isComplete = !note.isComplete;
      }
      return note;
    });
    this.observableNotes.next([...this.notes]);
  };
}
