# Angular Sandbox

## Requirement

Applications needed to work on this project :

- Any IDE (WebStorm, PHPStorm, Visual Studio Code, Atom, Vim, ...)
- Node.js + npm (or yarn)
- A browser

## Installation
After the installation of requirements, you have to install node dependancies. For this step, run one of the following command :
- `npm i`
- `yarn`

## Development server
Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.
The angular-compiler-cli module will watch any update of the project and update served resources.


## Running unit tests
The project is using Jasmine for Unit Tests.
Thank to update and maintained those tests up to date.
Jasmine is managed by Karma (that allows you to inspect tests as project sources).
<br/>For more information about [Karma](https://karma-runner.github.io).

Run `ng test` to execute the unit tests via
